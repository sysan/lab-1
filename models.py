import math


class Ferhults_model:
    def __init__(self, mu, limit_quantity, init_quantity):
        self.mu = mu
        self.limit_quantity = limit_quantity
        self.init_quantity = init_quantity

    def function(self):
        mu = self.mu
        limit_quantity = self.limit_quantity

        def f(t, y):
            return mu * y * (limit_quantity - y)

        return f


class ForcedOscillationModel:
    def __init__(self, delta, w0, f0, w):
        self.delta = delta
        self.w0 = w0
        self.f0 = f0
        self.w = w

    def function(self):
        def f(t, x, x_1):
            return self.f0 * math.cos(self.w * t) - self.w0 * self.w0 * x - 2 * x_1 * self.delta

        return f


class Solow_model:
    def __init__(self):
        pass

    def set_parameters(self, k0, s, a, mu, q, alpha):
        self.k0 = k0
        self.s = s
        self.a = a
        self.mu = mu
        self.q = q
        self.alpha = alpha

    def function(self):
        s = self.s
        a = self.a
        mu = self.mu
        q = self.q
        alpha = self.alpha

        def f(t, y):
            return s * a * (y ** alpha) - (mu + q) * y

        return f


class PredatorVictim_model:
    def __init__(self, a1, b1, a2, b2):
        self.a1 = a1
        self.b1 = b1
        self.a2 = a2
        self.b2 = b2

    def x_function(self):
        def f(x, y):
            return (self.a1 - self.b1 * y) * x

        return f

    def y_function(self):
        def f(x, y):
            return (-self.a2 + self.b2 * x) * y

        return f
