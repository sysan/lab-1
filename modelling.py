import matplotlib.pyplot as plt

from GUI import inputExplorer
from models import *
from solving import *


def Ferhults_demonstrate():
    fig, ax = plt.subplots(1)

    def plotDynamics(mu, limit_quantity, init_quantity):
        new = Ferhults_model(mu=mu, limit_quantity=limit_quantity, init_quantity=init_quantity)
        rkm = Runge_Kutta_factory.getRungeKutta(function=new.function(), init_value_of_t=0, init_value_of_y=new.init_quantity)
        result = rkm.solve()
        ax.clear()
        ax.plot(result[0][:], result[1][:], 'r')
        fig.canvas.draw()

    sliders = [{'label': label, 'valmin': 0, 'valmax': 100}
               for label in ['mu', 'k', 'N_0']]

    inputExplorer(plotDynamics, sliders)


def ForcedOscillation_demonstrate():
    fig, ax = plt.subplots(1)
    fig1, ax1 = plt.subplots(1)

    def plotDynamics(delta, w0, f0, w):
        new = ForcedOscillationModel(delta, w0, f0, w)
        rkm = Runge_Kutta_factory.getRungeKutta(function=new.function(),
                                                init_value_of_t=0,
                                                init_value_of_y=10,
                                                order=2)
        result = rkm.solve()
        ax.clear()
        ax.plot(result[0][:], result[1][:], 'r')

        fig.canvas.draw()
        ax1.clear()
        ax1.plot(result[1][:], result[2][:], 'r')
        fig1.canvas.draw()

    sliders = [{'label': label, 'valmin': 0, 'valmax': 100}
               for label in ['delta', 'w0', 'f0', 'w']]

    inputExplorer(plotDynamics, sliders)


def Solow_demonstrate():
    fig, ax = plt.subplots(1)

    def plotDynamics(k0, s, a, mu, q, alpha):
        new = Solow_model()
        new.set_parameters(k0=k0, s=s, a=a, mu=mu, q=q, alpha=alpha)
        rkm = Runge_Kutta_factory.getRungeKutta(function=new.function(), init_value_of_t=0, init_value_of_y=new.k0)
        result = rkm.solve()
        ax.clear()
        ax.plot(result[0][:], result[1][:], 'r')
        fig.canvas.draw()

    sliders = [{'label': label, 'valmin': 0, 'valmax': 2.5}
               for label in ['k0', 's', 'a', 'mu', 'q', 'alpha']]

    inputExplorer(plotDynamics, sliders)


def PredatorVictim_demonstrate():
    # plt.xlim(0, 35)
    # plt.autoscale(False)
    fig, ax = plt.subplots(1)
    fig1, ax1 = plt.subplots(1)

    def plotDynamics(a1, b1, a2, b2, x0, y0):
        new = PredatorVictim_model(a1, b1, a2, b2)
        rkm = Runge_Kutta_X_Y(function_x=new.x_function(),
                              function_y=new.y_function(),
                              init_value_of_t=0,
                              init_value_of_x=x0,
                              init_value_of_y=y0)
        result = rkm.solve()
        ax.clear()
        # ax.set_xlim([0, 35])
        ax.plot(result[0][:], result[1][:], 'b')
        ax.plot(result[0][:], result[2][:], 'r')
        fig.canvas.draw()

        ax1.clear()
        ax1.plot(result[1][:], result[2][:], 'r')
        fig1.canvas.draw()


    sliders = [{'label': label, 'valmin': 0, 'valmax': 10}
               for label in ['a1', 'b1', 'a2', 'b2', 'x0', 'y0']]

    inputExplorer(plotDynamics, sliders)

PredatorVictim_demonstrate()
# Ferhults_demonstrate()
# ForcedOscillation_demonstrate()
# Solow_demonstrate()
