import abc

import numpy as np


class Runge_Kutta_factory:
    @staticmethod
    def getRungeKutta(function, init_value_of_t, init_value_of_y, step=0.05, right_end_of_boundary=100, order=1):
        if order == 1:
            return Runge_Kutta_1(function, init_value_of_t, init_value_of_y, step, right_end_of_boundary)
        elif order == 2:
            return Runge_Kutta_2(function, init_value_of_t, init_value_of_y, step, right_end_of_boundary)


class Runge_Kutta(abc.ABC):
    def __init__(self, function, init_value_of_t, init_value_of_y, step, right_end_of_boundary):
        self.function = function
        self.init_value_of_t = init_value_of_t
        self.init_value_of_y = init_value_of_y
        self.step = step
        self.right_end_of_boundary = right_end_of_boundary

    @abc.abstractmethod
    def solve(self):
        pass


class Runge_Kutta_1(Runge_Kutta):
    def __init__(self, function, init_value_of_t, init_value_of_y, step, right_end_of_boundary):
        super().__init__(function, init_value_of_t, init_value_of_y, step, right_end_of_boundary)
        gridT = np.arange(self.init_value_of_t, self.right_end_of_boundary + self.step, self.step)
        self.grid = np.zeros((2, len(gridT)))
        self.grid[0][:] = gridT
        self.grid[1][0] = init_value_of_y

    def solve(self):
        u"""Розв'язок для диференційних рівнянь першого порядку"""
        for i in range(len(self.grid[0][:]) - 1):
            k1 = self.function(self.grid[0][i], self.grid[1][i])
            k2 = self.function(self.grid[0][i] + self.step / 2.0, self.grid[1][i] + self.step / 2.0 * k1)
            k3 = self.function(self.grid[0][i] + self.step / 2.0, self.grid[1][i] + self.step / 2.0 * k2)
            k4 = self.function(self.grid[0][i] + self.step / 2.0, self.grid[1][i] + self.step * k3)
            self.grid[1][i + 1] = self.grid[1][i] + (self.step / 6.0) * (k1 + 2 * k2 + 2 * k3 + k4)
        return self.grid


class Runge_Kutta_2(Runge_Kutta):
    def __init__(self, function, init_value_of_t, init_value_of_y, step=0.1, right_end_of_boundary=100):
        super().__init__(function, init_value_of_t, init_value_of_y, step, right_end_of_boundary)
        gridT = np.arange(self.init_value_of_t, self.right_end_of_boundary + self.step, self.step)
        self.grid = np.zeros((3, len(gridT)))
        self.grid[0][:] = gridT
        self.grid[1][0] = 3
        self.grid[2][0] = 2

    def solve(self):
        u""" Розв'язок для диференційних рівнянь другого порядку"""
        step = self.step
        for i in range(len(self.grid[0][:]) - 1):
            t = self.grid[0][i]
            x = self.grid[1][i]
            y = self.grid[2][i]
            k1 = self.function(t, x, y)
            k2 = self.function(t, x + self.step / 2, y + step / 2.0 * k1)
            k3 = self.function(t, x + self.step / 2, y + step / 2.0 * k2)
            k4 = self.function(t, x + self.step, y + step * k3)
            self.grid[2][i + 1] = y + (step / 6) * (k1 + 2 * k2 + 2 * k3 + k4)

            y = self.grid[2][i + 1]
            xk1 = y
            xk2 = y + step * xk1 / 2
            xk3 = y + step * xk2 / 2
            xk4 = y + step * xk2
            self.grid[1][i + 1] = self.grid[1][i] + (step / 6) * (xk1 + 2 * xk2 + 2 * xk3 + xk4)

        return self.grid


class Runge_Kutta_X_Y:
    def __init__(self, function_x, function_y, init_value_of_t, init_value_of_x, init_value_of_y, step=0.1,
                 right_end_of_boundary=100):
        self.function_x = function_x
        self.function_y = function_y
        self.step = step
        gridT = np.arange(init_value_of_t, right_end_of_boundary + self.step, self.step)
        self.grid = np.zeros((3, len(gridT)))
        self.grid[0][:] = gridT
        self.grid[1][0] = init_value_of_x
        self.grid[2][0] = init_value_of_y

    def solve(self):
        step = self.step
        for i in range(len(self.grid[0][:]) - 1):
            x = self.grid[1][i]
            y = self.grid[2][i]
            h = self.step

            k1 = h * self.function_x(x, y)
            l1 = h * self.function_y(x, y)
            k2 = h * self.function_x(x + k1 / 2, y + l1 / 2)
            l2 = h * self.function_y(x + k1 / 2, y + l1 / 2)
            k3 = h * self.function_x(x + k2 / 2, y + l2 / 2)
            l3 = h * self.function_y(x + k2 / 2, y + l2 / 2)
            k4 = h * self.function_x(x + k3, y + l3)
            l4 = h * self.function_y(x + k3, y + l3)

            self.grid[1][i + 1] = x + (1/ 6) * (k1 + 2 * k2 + 2 * k3 + k4)
            self.grid[2][i + 1] = y + (1 / 6) * (l1 + 2 * l2 + 2 * l3 + l4)

        return self.grid
